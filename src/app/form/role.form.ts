import { AbstractControl, FormControl, Validators } from '@angular/forms';

export const FORM_ROLE: {[key:string]: AbstractControl} = {
    id : new FormControl(''),
    pilotId: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required]),
}
