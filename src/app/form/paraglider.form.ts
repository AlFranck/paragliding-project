import { AbstractControl, FormControl, Validators } from '@angular/forms';

export const FORM_PARAGLIDER: {[key:string]: AbstractControl} = {
    id : new FormControl(''),
    name: new FormControl('', [Validators.required]),
    commissioningDate: new FormControl('', [Validators.required]),
    lastRevision: new FormControl('', [Validators.required]),
    paragliderModelId: new FormControl('', [Validators.required]),
}