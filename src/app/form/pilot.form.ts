import { AbstractControl, FormControl, Validators } from '@angular/forms';

export const FORM_PILOT: {[key:string]: AbstractControl} = {
    id : new FormControl(''),
    firstName: new FormControl('', [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    address: new FormControl('', [Validators.required]),
    phone: new FormControl('', [Validators.required]),
}
