import { TestBed } from '@angular/core/testing';

import { ParagliderService } from './paraglider.service';

describe('ParagliderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParagliderService = TestBed.get(ParagliderService);
    expect(service).toBeTruthy();
  });
});
