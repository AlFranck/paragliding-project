import { TestBed } from '@angular/core/testing';

import { ParagliderModelService } from './paraglider-model.service';

describe('ParagliderModelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParagliderModelService = TestBed.get(ParagliderModelService);
    expect(service).toBeTruthy();
  });
});
