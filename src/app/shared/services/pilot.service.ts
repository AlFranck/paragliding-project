import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pilot } from '../models/pilot';

@Injectable({
  providedIn: 'root'
})
export class PilotService {

  constructor(private httpClient:HttpClient) { }

  getPilots() : Observable<Pilot[]>{
    return this.httpClient.get<Pilot[]>(`/api/pilotes`);
  }

  addPilot(pilot:Pilot): Observable<Pilot> {
    return this.httpClient.post<Pilot>(`/api/pilotes`, pilot);
  }

  deletePilot(id:number) : Observable<Pilot> {
    return this.httpClient.delete<Pilot>(`/api/pilotes/${id}`);
  }

  editPilot(pilot:Pilot) : Observable<Pilot> {
    return this.httpClient.put<Pilot>(`/api/pilotes/${pilot.id}`, pilot);
  }

  getPilot(id:string) : Observable<Pilot> {
    return this.httpClient.get<Pilot>(`/api/pilotes/${id}`);
  }

}
