import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Paraglider } from '../models/paraglider';

@Injectable({
  providedIn: 'root'
})
export class ParagliderService {

  constructor(private httpClient:HttpClient) { }

  getParagliders() : Observable<Paraglider[]>{
    return this.httpClient.get<Paraglider[]>(`/api/paragliders`);
  }

  getParaglider(id:string) : Observable<Paraglider>{
    return this.httpClient.get<Paraglider>(`/api/paragliders/${id}`);
  }

  addParaglider(paraglider:Paraglider) : Observable<Paraglider>{
    return this.httpClient.post<Paraglider>(`/api/paragliders`, paraglider);
  }

  editParaglider(paraglider:Paraglider) : Observable<Paraglider>{
    return this.httpClient.put<Paraglider>(`/api/paragliders/${paraglider.id}`, paraglider);
  }

  deleteParaglider(id:number) : Observable<Paraglider>{
    return this.httpClient.delete<Paraglider>(`/api/paragliders/${id}`);
  }


}
