import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ParagliderModel } from '../models/paragliderModel';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ParagliderModelService {

  constructor(private httpClient: HttpClient) { }

  getParagliderModels() : Observable<ParagliderModel[]> {
    return this.httpClient.get<ParagliderModel[]>(`/api/paragliderModels`);
  }

  getParagliderModel(id:string) : Observable<ParagliderModel> {
    return this.httpClient.get<ParagliderModel>(`/api/paragliderModels/${id}`);
  }

  deleteParagliderModel(id:number) : Observable<ParagliderModel> {
    return this.httpClient.delete<ParagliderModel>(`/api/paragliderModels/${id}`);
  }

  addParagliderModel(paragliderModel:ParagliderModel) : Observable<ParagliderModel> {
    return this.httpClient.post<ParagliderModel>(`/api/paragliderModels`, paragliderModel);
  }

  editParagliderModel(paragliderModel:ParagliderModel) : Observable<ParagliderModel> {
    return this.httpClient.put<ParagliderModel>(`/api/paragliderModels/${paragliderModel.id}`, paragliderModel);
  }
}
