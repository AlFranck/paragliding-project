import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Role } from '../models/role';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private httpClient: HttpClient) { }

  getRoles() : Observable<Role[]>{
    return this.httpClient.get<Role[]>(`/api/roles`);
  }

  getRole(id:string) : Observable<Role>{
    return this.httpClient.get<Role>(`/api/roles/${id}`);
  }

  deleteRole(id:number) : Observable<Role>{
    return this.httpClient.delete<Role>(`/api/roles/${id}`);
  }

  editRole(role:Role) : Observable<Role>{
    return this.httpClient.put<Role>(`/api/roles/${role.id}`, role);
  }

  addRole(role:Role) : Observable<Role>{
    return this.httpClient.post<Role>(`/api/roles`, role);
  }
}
