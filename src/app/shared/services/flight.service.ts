import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Flight } from '../models/flight';

@Injectable({
  providedIn: 'root'
})
export class FlightService {

  constructor(private httpClient:HttpClient) { }

  getFlights() : Observable<Flight[]>{
    return this.httpClient.get<Flight[]>(`/api/flights`);
  }

  getFlight(id:number) : Observable<Flight>{
    return this.httpClient.get<Flight>(`/api/flights/${id}`)
  }
  
  addFlight(flight:Flight) : Observable<Flight>{
    return this.httpClient.post<Flight>(`/api/flights`, flight);
  }

  deleteFlight(id:number) : Observable<Flight>{
    return this.httpClient.delete<Flight>(`/api/flights/${id}`);
  }

  editFlight(flight:Flight) : Observable<Flight>{
    return this.httpClient.put<Flight>(`/api/flights/${flight.id}`, flight);
  }
}
