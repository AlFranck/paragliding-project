import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Site } from '../models/site';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SiteService {

  constructor(private httpClient:HttpClient) { }

  getSites() : Observable<Site[]>{
    return this.httpClient.get<Site[]>(`/api/sites`);
  }

  getSite(id:string) : Observable<Site>{
    return this.httpClient.get<Site>(`/api/sites/${id}`);
  }

  addSite(site:Site) : Observable<Site>{
    return this.httpClient.post<Site>(`/api/sites`, site);
  }

  editSite(site:Site) : Observable<Site>{
    return this.httpClient.put<Site>(`/api/sites/${site.id}`, site);
  }

  deleteSite(id:number) : Observable<Site>{
    return this.httpClient.delete<Site>(`/api/sites/${id}`)
  }
}
