import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'convertToDate'
})
export class ConvertToDatePipe implements PipeTransform {

  constructor(private datePipe:DatePipe){}

  transform(value: Date, format:string): string {
    const date = new Date(value);
    return this.datePipe.transform(date, format);
  }

}
