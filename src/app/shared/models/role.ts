export interface Role {
    id: number;
    pilotId: number;
    name: string;
}