export interface ParagliderModel {
    id: number;
    size: string;
    minWeightPilot: number;
    maxWeightPilot:number;
    approvalNumber:string;
    approvalDate:Date;
}
