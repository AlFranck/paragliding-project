export interface Pilot {
    id: number;
    firstName: string;
    lastName: string;
    address:string;
    phone:Date;
}