export interface Flight {
    id: number;
    pilotId: number;
    flightDate: Date;
    duration:number;
    paragliderId:number;
    takeOffSiteId:number;
    landingSiteId:number;
}
