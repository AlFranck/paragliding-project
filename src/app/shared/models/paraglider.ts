export interface Paraglider {
    id: number;
    name: string;
    commissioningDate: Date;
    lastRevision:Date;
    paragliderModelId:number;
}
