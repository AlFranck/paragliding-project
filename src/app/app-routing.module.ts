import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'accueil',
    loadChildren: () => import('./pages/accueil/accueil.module').then( m => m.AccueilPageModule)
  },
  {
    path: '',
    redirectTo: 'accueil',
    pathMatch: 'full'
  },
  {
    path: 'pilots-list',
    loadChildren: () => import('./pages/pilots-list/pilots-list.module').then( m => m.PilotsListPageModule)
  },
  {
    path: 'pilot-add',
    loadChildren: () => import('./pages/pilot-add/pilot-add.module').then( m => m.PilotAddPageModule)
  },
  {
    path: 'pilot-edit',
    loadChildren: () => import('./pages/pilot-edit/pilot-edit.module').then( m => m.PilotEditPageModule)
  },
  {
    path: 'pilot-details',
    loadChildren: () => import('./pages/pilot-details/pilot-details.module').then( m => m.PilotDetailsPageModule)
  },
  {
    path: 'role-list',
    loadChildren: () => import('./pages/role-list/role-list.module').then( m => m.RoleListPageModule)
  },
  {
    path: 'role-details',
    loadChildren: () => import('./pages/role-details/role-details.module').then( m => m.RoleDetailsPageModule)
  },
  {
    path: 'role-add',
    loadChildren: () => import('./pages/role-add/role-add.module').then( m => m.RoleAddPageModule)
  },
  {
    path: 'role-edit',
    loadChildren: () => import('./pages/role-edit/role-edit.module').then( m => m.RoleEditPageModule)
  },
  {
    path: 'paragliders-list',
    loadChildren: () => import('./pages/paragliders-list/paragliders-list.module').then( m => m.ParaglidersListPageModule)
  },
  {
    path: 'paraglider-detail',
    loadChildren: () => import('./pages/paraglider-detail/paraglider-detail.module').then( m => m.ParagliderDetailPageModule)
  },
  {
    path: 'paraglider-add',
    loadChildren: () => import('./pages/paraglider-add/paraglider-add.module').then( m => m.ParagliderAddPageModule)
  },
  {
    path: 'paraglider-edit',
    loadChildren: () => import('./pages/paraglider-edit/paraglider-edit.module').then( m => m.ParagliderEditPageModule)
  },
  {
    path: 'flights-list',
    loadChildren: () => import('./pages/flights-list/flights-list.module').then( m => m.FlightsListPageModule)
  },
  {
    path: 'sites-list',
    loadChildren: () => import('./pages/sites-list/sites-list.module').then( m => m.SitesListPageModule)
  },
  {
    path: 'detailsites-list',
    loadChildren: () => import('./pages/detailsites-list/detailsites-list.module').then( m => m.DetailsitesListPageModule)
  },
  {
    path: 'site-detail',
    loadChildren: () => import('./pages/site-detail/site-detail.module').then( m => m.SiteDetailPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
