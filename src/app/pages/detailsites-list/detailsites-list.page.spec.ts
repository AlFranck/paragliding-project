import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailsitesListPage } from './detailsites-list.page';

describe('DetailsitesListPage', () => {
  let component: DetailsitesListPage;
  let fixture: ComponentFixture<DetailsitesListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsitesListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailsitesListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
