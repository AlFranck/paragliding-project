import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { SiteService } from '../../shared/services/site.service';
import { Site } from 'src/app/shared/models/site';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detailsites-list',
  templateUrl: './detailsites-list.page.html',
  styleUrls: ['./detailsites-list.page.scss'],
})
export class DetailsitesListPage implements OnInit {

  id:string;
  sites:Site[] = [];
  title:string = "";
  constructor(private navController:NavController, private serviceSite:SiteService, private route:ActivatedRoute) { }

  ngOnInit() {
      this.id = this.route.snapshot.params['id'];
      this.serviceSite.getSites().subscribe(datas => {
        this.sites = datas.filter(site => site.siteType.toString() == this.id);
        console.log(this.sites);
      })  

      if(this.id == "1"){
        this.title="de décollage";
      }
      else{
        this.title="d'atterrissage";
      }
  }

  back(){
    this.navController.back();
  }

}
