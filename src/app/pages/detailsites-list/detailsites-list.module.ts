import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsitesListPageRoutingModule } from './detailsites-list-routing.module';

import { DetailsitesListPage } from './detailsites-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsitesListPageRoutingModule
  ],
  declarations: [DetailsitesListPage]
})
export class DetailsitesListPageModule {}
