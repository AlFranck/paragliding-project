import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailsitesListPage } from './detailsites-list.page';

const routes: Routes = [
  {
    path: ':id',
    component: DetailsitesListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailsitesListPageRoutingModule {}
