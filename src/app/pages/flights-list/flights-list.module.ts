import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FlightsListPageRoutingModule } from './flights-list-routing.module';

import { FlightsListPage } from './flights-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FlightsListPageRoutingModule
  ],
  declarations: [FlightsListPage]
})
export class FlightsListPageModule {}
