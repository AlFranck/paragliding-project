import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { FlightService } from '../../shared/services/flight.service';
import { Flight } from 'src/app/shared/models/flight';
import { ParagliderService } from '../../shared/services/paraglider.service';
import { Paraglider } from 'src/app/shared/models/paraglider';
import { PilotService } from '../../shared/services/pilot.service';
import { Pilot } from 'src/app/shared/models/pilot';
import { SiteService } from '../../shared/services/site.service';

@Component({
  selector: 'app-flights-list',
  templateUrl: './flights-list.page.html',
  styleUrls: ['./flights-list.page.scss'],
})
export class FlightsListPage implements OnInit {

  id : string;
  flights:Flight[];
  paragliders:Paraglider[];
  constructor(private navController:NavController, private route:ActivatedRoute, private serviceFlight:FlightService, private serviceParaglider:ParagliderService, private serviceSite:SiteService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.serviceFlight.getFlights().subscribe(datas => this.flights = datas);
    this.serviceParaglider.getParagliders().subscribe(datas => this.paragliders = datas);
  }

  back() {
    this.navController.back();
  }

}
