import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FlightsListPage } from './flights-list.page';

describe('FlightsListPage', () => {
  let component: FlightsListPage;
  let fixture: ComponentFixture<FlightsListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightsListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FlightsListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
