import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FlightsListPage } from './flights-list.page';

const routes: Routes = [
  {
    path: ':id',
    component: FlightsListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FlightsListPageRoutingModule {}
