import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SiteService } from '../../shared/services/site.service';
import { Site } from 'src/app/shared/models/site';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-site-detail',
  templateUrl: './site-detail.page.html',
  styleUrls: ['./site-detail.page.scss'],
})
export class SiteDetailPage implements OnInit {

  id:string;
  site:Site;
  constructor(private route:ActivatedRoute, private serviceSite:SiteService, private navController:NavController) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.serviceSite.getSite(this.id).subscribe(data => this.site = data);
  }

  back(){
    this.navController.back();
  }

}
