import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PilotAddPage } from './pilot-add.page';

describe('PilotAddPage', () => {
  let component: PilotAddPage;
  let fixture: ComponentFixture<PilotAddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PilotAddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PilotAddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
