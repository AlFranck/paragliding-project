import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PilotAddPageRoutingModule } from './pilot-add-routing.module';

import { PilotAddPage } from './pilot-add.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PilotAddPageRoutingModule, 
    ReactiveFormsModule
  ],
  declarations: [PilotAddPage]
})
export class PilotAddPageModule {}
