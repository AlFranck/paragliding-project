import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FORM_PILOT } from '../../form/pilot.form';
import { PilotService } from '../../shared/services/pilot.service';

@Component({
  selector: 'app-pilot-add',
  templateUrl: './pilot-add.page.html',
  styleUrls: ['./pilot-add.page.scss'],
})
export class PilotAddPage implements OnInit {

  form:FormGroup;

  constructor(private navController: NavController, private formBuilder: FormBuilder, private pilotService: PilotService) { }

  ngOnInit() {
    this.form = this.formBuilder.group(FORM_PILOT);
  }

  add() {
    console.log(this.form.value)
    if(this.form.valid){
      this.pilotService.addPilot(this.form.value).subscribe();
      this.navController.back();
    }
  }

  back() {
    this.navController.back();
  }

}
