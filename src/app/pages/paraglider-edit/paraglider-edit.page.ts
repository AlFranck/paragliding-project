import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ParagliderService } from '../../shared/services/paraglider.service';
import { Paraglider } from 'src/app/shared/models/paraglider';
import { ParagliderModelService } from '../../shared/services/paraglider-model.service';
import { ParagliderModel } from 'src/app/shared/models/paragliderModel';
import { FORM_PARAGLIDER } from 'src/app/form/paraglider.form';

@Component({
  selector: 'app-paraglider-edit',
  templateUrl: './paraglider-edit.page.html',
  styleUrls: ['./paraglider-edit.page.scss'],
})
export class ParagliderEditPage implements OnInit {

  form:FormGroup;
  id:string;
  paraglider:Paraglider;
  paragliderModel:ParagliderModel;
  paragliderModels:ParagliderModel[];
  constructor(private navController:NavController, private formBuilder:FormBuilder, private route:ActivatedRoute, private serviceParaglider:ParagliderService, private serviceModelParaglider:ParagliderModelService) { }

  ngOnInit() {
    this.form = this.formBuilder.group(FORM_PARAGLIDER);
  }

  ionViewDidEnter() {
    this.id = this.route.snapshot.params['id'];
    this.serviceModelParaglider.getParagliderModels().subscribe(datas => this.paragliderModels = datas);
    this.serviceParaglider.getParaglider(this.id).subscribe(data => {
      this.paraglider = data;
      this.form.patchValue(this.paraglider);
      this.serviceModelParaglider.getParagliderModel(this.paraglider.paragliderModelId.toString()).subscribe(data => this.paragliderModel = data);
    } )

  }

  edit(){
    if(this.form.valid){
      this.serviceParaglider.editParaglider(this.form.value).subscribe();
      this.navController.back();
    }
  }

  back() {
    this.navController.back();
  }

}
