import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParagliderEditPage } from './paraglider-edit.page';

describe('ParagliderEditPage', () => {
  let component: ParagliderEditPage;
  let fixture: ComponentFixture<ParagliderEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParagliderEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParagliderEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
