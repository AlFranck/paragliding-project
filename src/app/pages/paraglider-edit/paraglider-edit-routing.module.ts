import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParagliderEditPage } from './paraglider-edit.page';

const routes: Routes = [
  {
    path: ':id',
    component: ParagliderEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParagliderEditPageRoutingModule {}
