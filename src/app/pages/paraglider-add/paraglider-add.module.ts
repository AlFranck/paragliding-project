import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParagliderAddPageRoutingModule } from './paraglider-add-routing.module';

import { ParagliderAddPage } from './paraglider-add.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParagliderAddPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ParagliderAddPage]
})
export class ParagliderAddPageModule {}
