import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParagliderAddPage } from './paraglider-add.page';

describe('ParagliderAddPage', () => {
  let component: ParagliderAddPage;
  let fixture: ComponentFixture<ParagliderAddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParagliderAddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParagliderAddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
