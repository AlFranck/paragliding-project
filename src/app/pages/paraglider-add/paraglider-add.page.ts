import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ParagliderService } from 'src/app/shared/services/paraglider.service';
import { ParagliderModelService } from '../../shared/services/paraglider-model.service';
import { ParagliderModel } from 'src/app/shared/models/paragliderModel';
import { FORM_PARAGLIDER } from 'src/app/form/paraglider.form';

@Component({
  selector: 'app-paraglider-add',
  templateUrl: './paraglider-add.page.html',
  styleUrls: ['./paraglider-add.page.scss'],
})
export class ParagliderAddPage implements OnInit {

  form:FormGroup;
  paragliderModels:ParagliderModel[];
  constructor(private navController:NavController, private formBuilder:FormBuilder, private serviceParaglider:ParagliderService, private serviceParagliderModel:ParagliderModelService) { }

  ngOnInit() {
    this.form = this.formBuilder.group(FORM_PARAGLIDER);
    this.serviceParagliderModel.getParagliderModels().subscribe(datas => this.paragliderModels = datas);
    
  }


  add(){
    if(this.form.valid){
      this.serviceParaglider.addParaglider(this.form.value).subscribe();
      this.navController.back();
    }
  }

  back() {
    this.navController.back();
  }

}
