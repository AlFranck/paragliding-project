import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FORM_PILOT } from 'src/app/form/pilot.form';
import { ActivatedRoute } from '@angular/router';
import { PilotService } from '../../shared/services/pilot.service';
import { Pilot } from '../../shared/models/pilot';

@Component({
  selector: 'app-pilot-edit',
  templateUrl: './pilot-edit.page.html',
  styleUrls: ['./pilot-edit.page.scss'],
})
export class PilotEditPage implements OnInit {

  form : FormGroup; 
  id : string;
  pilot : Pilot;

  constructor(private navController:NavController, private servicePilot: PilotService, private formBuilder:FormBuilder, private route:ActivatedRoute) { }

  ngOnInit() {
    this.form = this.formBuilder.group(FORM_PILOT);
    
  }

  ionViewDidEnter() {
    this.id = this.route.snapshot.params['id'];
    this.servicePilot.getPilot(this.id).subscribe(data => {this.pilot = data; this.form.patchValue(this.pilot);});
  }

  edit(){
    console.log(this.form.value)
    if(this.form.valid){
      this.servicePilot.editPilot(this.form.value).subscribe();
      this.navController.back();
    }
  }

  back() {
    this.navController.back();
  }

}
