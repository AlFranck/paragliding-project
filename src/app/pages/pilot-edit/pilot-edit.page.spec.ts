import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PilotEditPage } from './pilot-edit.page';

describe('PilotEditPage', () => {
  let component: PilotEditPage;
  let fixture: ComponentFixture<PilotEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PilotEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PilotEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
