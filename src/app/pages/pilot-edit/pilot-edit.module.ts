import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PilotEditPageRoutingModule } from './pilot-edit-routing.module';

import { PilotEditPage } from './pilot-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PilotEditPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [PilotEditPage]
})
export class PilotEditPageModule {}
