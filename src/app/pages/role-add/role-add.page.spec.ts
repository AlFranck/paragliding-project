import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RoleAddPage } from './role-add.page';

describe('RoleAddPage', () => {
  let component: RoleAddPage;
  let fixture: ComponentFixture<RoleAddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleAddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RoleAddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
