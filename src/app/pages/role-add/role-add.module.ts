import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoleAddPageRoutingModule } from './role-add-routing.module';

import { RoleAddPage } from './role-add.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoleAddPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [RoleAddPage]
})
export class RoleAddPageModule {}
