import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FORM_ROLE } from '../../form/role.form';
import { RoleService } from '../../shared/services/role.service';
import { PilotService } from '../../shared/services/pilot.service';
import { Pilot } from 'src/app/shared/models/pilot';

@Component({
  selector: 'app-role-add',
  templateUrl: './role-add.page.html',
  styleUrls: ['./role-add.page.scss'],
})
export class RoleAddPage implements OnInit {

  form:FormGroup;
  pilots:Pilot[];
  constructor(private navController:NavController, private formBuilder: FormBuilder, private serviceRole:RoleService, private servicePilot:PilotService) { }

  ngOnInit() {
    this.form = this.formBuilder.group(FORM_ROLE);
    this.servicePilot.getPilots().subscribe(datas => this.pilots = datas);
  }

  add() {
    console.log(this.form.value)
    if(this.form.valid){
      this.serviceRole.addRole(this.form.value).subscribe();
      this.navController.back();
    }
  }

  back(){
    this.navController.back();
  }

}
