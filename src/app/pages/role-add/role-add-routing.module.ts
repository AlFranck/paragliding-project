import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoleAddPage } from './role-add.page';

const routes: Routes = [
  {
    path: '',
    component: RoleAddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoleAddPageRoutingModule {}
