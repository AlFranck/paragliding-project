import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoleEditPageRoutingModule } from './role-edit-routing.module';

import { RoleEditPage } from './role-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoleEditPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [RoleEditPage]
})
export class RoleEditPageModule {}
