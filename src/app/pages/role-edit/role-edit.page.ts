import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FORM_ROLE } from 'src/app/form/role.form';
import { RoleService } from '../../shared/services/role.service';
import { PilotService } from '../../shared/services/pilot.service';
import { Role } from 'src/app/shared/models/role';
import { Pilot } from 'src/app/shared/models/pilot';
import { FORM_PILOT } from 'src/app/form/pilot.form';

@Component({
  selector: 'app-role-edit',
  templateUrl: './role-edit.page.html',
  styleUrls: ['./role-edit.page.scss'],
})
export class RoleEditPage implements OnInit {

  id:string;
  form:FormGroup;
  role:Role;
  pilot:Pilot;
  pilots:Pilot[];
  constructor(private navController:NavController, private route:ActivatedRoute, private formBuilder:FormBuilder, private serviceRole:RoleService, private servicePilot:PilotService) { }

  ngOnInit() {
    this.form = this.formBuilder.group(FORM_ROLE);
   

    
  }

  ionViewDidEnter(){
    this.id = this.route.snapshot.params['id'];
    this.servicePilot.getPilots().subscribe(datas => this.pilots = datas); 
    this.serviceRole.getRole(this.id).subscribe(data =>  {
      this.role = data; 
       this.form.patchValue(this.role); 
      this.servicePilot.getPilot(this.role.pilotId.toString()).subscribe(data => this.pilot = data);
    } );
  }


  edit(){
    if(this.form.valid){
      this.serviceRole.editRole(this.form.value).subscribe();
      this.navController.back();
    }
  }

  back(){
    this.navController.back();
  }

}
