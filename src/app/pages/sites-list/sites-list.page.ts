import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { SiteService } from '../../shared/services/site.service';
import { Site } from 'src/app/shared/models/site';

@Component({
  selector: 'app-sites-list',
  templateUrl: './sites-list.page.html',
  styleUrls: ['./sites-list.page.scss'],
})
export class SitesListPage implements OnInit {

  numberOfTakeOffSites:number = 0;
  numberOfLandingSites:number = 0;
  sites : Site[] = []
  constructor(private navController:NavController, private serviceSite:SiteService) { }

  ngOnInit() {
    this.serviceSite.getSites().subscribe(datas => {
      this.sites = datas;
      this.numberOfTakeOffSites = this.sites.filter(site => site.siteType == 1).length;
      this.numberOfLandingSites = this.sites.filter(site => site.siteType == 2).length;
    });

  }

  back(){
    this.navController.back();
  }

}
