import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoleDetailsPage } from './role-details.page';

const routes: Routes = [
  {
    path: ':id',
    component: RoleDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoleDetailsPageRoutingModule {}
