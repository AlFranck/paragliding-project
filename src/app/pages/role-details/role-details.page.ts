import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RoleService } from '../../shared/services/role.service';
import { Role } from 'src/app/shared/models/role';
import { NavController } from '@ionic/angular';
import { PilotService } from 'src/app/shared/services/pilot.service';
import { Pilot } from 'src/app/shared/models/pilot';
import { Flight } from 'src/app/shared/models/flight';
import { FlightService } from '../../shared/services/flight.service';

@Component({
  selector: 'app-role-details',
  templateUrl: './role-details.page.html',
  styleUrls: ['./role-details.page.scss'],
})
export class RoleDetailsPage implements OnInit {

  id:string;
  role:Role;
  pilot:Pilot;
  flights:Flight[];
  flightsPilot:Flight[];
  numberOfFlight:number;
  route:ActivatedRoute;
  constructor(private servicePilot:PilotService, private _route:ActivatedRoute, private serviceRole:RoleService, private navController:NavController) { 
    this.route = _route;
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.serviceRole.getRole(this.id).subscribe(data => {
      this.role = data;
      this.servicePilot.getPilot(this.role.pilotId.toString()).subscribe(data => this.pilot = data)
    });

    

  }

  back(){
    this.navController.back();
  }

}
