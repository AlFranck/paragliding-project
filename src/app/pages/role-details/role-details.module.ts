import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoleDetailsPageRoutingModule } from './role-details-routing.module';

import { RoleDetailsPage } from './role-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoleDetailsPageRoutingModule
  ],
  declarations: [RoleDetailsPage]
})
export class RoleDetailsPageModule {}
