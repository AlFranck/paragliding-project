import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParaglidersListPage } from './paragliders-list.page';

describe('ParaglidersListPage', () => {
  let component: ParaglidersListPage;
  let fixture: ComponentFixture<ParaglidersListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParaglidersListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParaglidersListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
