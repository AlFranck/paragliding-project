import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ParagliderService } from '../../shared/services/paraglider.service';
import { Paraglider } from 'src/app/shared/models/paraglider';

@Component({
  selector: 'app-paragliders-list',
  templateUrl: './paragliders-list.page.html',
  styleUrls: ['./paragliders-list.page.scss'],
})
export class ParaglidersListPage implements OnInit {

  paragliders:Paraglider[];
  constructor(private navController:NavController, private serviceParaglider:ParagliderService) { }

  ngOnInit() {
    this.serviceParaglider.getParagliders().subscribe(datas => this.paragliders = datas);
  }

  back(){
    this.navController.back();
  }

  deleteParaglider(id:number){
    this.serviceParaglider.deleteParaglider(id).subscribe();
    location.reload();
  }

}
