import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParaglidersListPageRoutingModule } from './paragliders-list-routing.module';

import { ParaglidersListPage } from './paragliders-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParaglidersListPageRoutingModule
  ],
  declarations: [ParaglidersListPage]
})
export class ParaglidersListPageModule {}
