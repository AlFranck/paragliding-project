import { Component, OnInit } from '@angular/core';
import { RoleService } from '../../shared/services/role.service';
import { Role } from 'src/app/shared/models/role';
import { PilotService } from 'src/app/shared/services/pilot.service';
import { Pilot } from 'src/app/shared/models/pilot';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.page.html',
  styleUrls: ['./role-list.page.scss'],
})
export class RoleListPage implements OnInit {

  constructor(private serviceRole: RoleService, private navController:NavController) { }

  roles:Role[];
  ngOnInit() {
  }

  ionViewDidEnter() {
    this.serviceRole.getRoles().subscribe(datas => this.roles = datas);
  }

  back() {
    this.navController.back();
  }

  deleteRole(id:number){
    this.serviceRole.deleteRole(id).subscribe();
    location.reload();
  }

}
