import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PilotDetailsPageRoutingModule } from './pilot-details-routing.module';

import { PilotDetailsPage } from './pilot-details.page';
import { ConvertToDatePipe } from '../../shared/pipes/convert-to-date.pipe';
import { AppModule } from '../../app.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PilotDetailsPageRoutingModule
  ],
  providers: [DatePipe],
  declarations: [PilotDetailsPage]
})
export class PilotDetailsPageModule {}
