import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { PilotService } from '../../shared/services/pilot.service';
import { FlightService } from '../../shared/services/flight.service';
import { Pilot } from '../../shared/models/pilot';
import { Flight } from '../../shared/models/flight';


@Component({
  selector: 'app-pilot-details',
  templateUrl: './pilot-details.page.html',
  styleUrls: ['./pilot-details.page.scss'],
})
export class PilotDetailsPage implements OnInit {

  id: string;
  pilot : Pilot;
  flights : Flight[] = [];
  flight:Flight;
  numberOfFlights:number = 0;
  constructor(private navController: NavController, private route:ActivatedRoute, private servicePilot: PilotService, private serviceFlight:FlightService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.servicePilot.getPilot(this.id).subscribe(data => this.pilot = data);
    this.serviceFlight.getFlights().subscribe(datas =>
       {this.flights = datas;
        this.numberOfFlights = this.flights.filter(flight => flight.pilotId == this.pilot.id).length;
      });
  

    
  }

  back() {
    this.navController.back();
  }

}
