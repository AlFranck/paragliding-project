import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParagliderDetailPageRoutingModule } from './paraglider-detail-routing.module';

import { ParagliderDetailPage } from './paraglider-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParagliderDetailPageRoutingModule
  ],
  declarations: [ParagliderDetailPage]
})
export class ParagliderDetailPageModule {}
