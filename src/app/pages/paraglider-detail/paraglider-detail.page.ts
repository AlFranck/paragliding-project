import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { ParagliderService } from '../../shared/services/paraglider.service';
import { Paraglider } from 'src/app/shared/models/paraglider';
import { ParagliderModelService } from '../../shared/services/paraglider-model.service';
import { ParagliderModel } from 'src/app/shared/models/paragliderModel';

@Component({
  selector: 'app-paraglider-detail',
  templateUrl: './paraglider-detail.page.html',
  styleUrls: ['./paraglider-detail.page.scss'],
})
export class ParagliderDetailPage implements OnInit {

  id:string;
  paraglider:Paraglider;
  paragliderModel:ParagliderModel;
  constructor(private navController:NavController, private route:ActivatedRoute, private serviceParaglider:ParagliderService, private serviceParagliderModel:ParagliderModelService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.serviceParaglider.getParaglider(this.id).subscribe(data => {
      this.paraglider = data;
      this.serviceParagliderModel.getParagliderModel(this.paraglider.paragliderModelId.toString()).subscribe(data => this.paragliderModel = data);

    });
  }

  back(){
    this.navController.back();
  }

}
