import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PilotsListPage } from './pilots-list.page';

describe('PilotsListPage', () => {
  let component: PilotsListPage;
  let fixture: ComponentFixture<PilotsListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PilotsListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PilotsListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
