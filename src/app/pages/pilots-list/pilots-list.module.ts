import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PilotsListPageRoutingModule } from './pilots-list-routing.module';

import { PilotsListPage } from './pilots-list.page';
import { SharedModule } from '../../../../../techni_ionic_demo/src/app/shared/shared.module';
import {HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PilotsListPageRoutingModule,
    SharedModule
  ],
  declarations: [PilotsListPage]
})
export class PilotsListPageModule {}
