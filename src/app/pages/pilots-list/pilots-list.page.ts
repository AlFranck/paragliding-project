import { Component, OnInit } from '@angular/core';
import { Pilot } from '../../shared/models/pilot';
import { PilotService } from '../../shared/services/pilot.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-pilots-list',
  templateUrl: './pilots-list.page.html',
  styleUrls: ['./pilots-list.page.scss'],
})
export class PilotsListPage implements OnInit {

  pilots: Pilot[] = [];

  constructor(private servicePilot:PilotService, private navController:NavController) { }

  ngOnInit() {
    
  }

  ionViewDidEnter() {
    this.servicePilot.getPilots().subscribe(datas => this.pilots = datas);
  }

  deletePilot(id:number) {
    this.servicePilot.deletePilot(id).subscribe();
    location.reload();
  }

  back() {
    this.navController.back();
  }

}
