import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PilotsListPage } from './pilots-list.page';

const routes: Routes = [
  {
    path: '',
    component: PilotsListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PilotsListPageRoutingModule {}
